ensure_user_present:
  vultr.user.present:
    - name: idemcloud testuser
    - email: tester@idemcloud.com

ensure_startup_script:
  vultr.script.present:
    - name: startup.sh
    - pxe: False
    # If the path exists to the content, the file will be read, otherwise this string will be sent as the content
    - content: |
        \#!/bin/bash
        echo "Starting VM!"

# If I need a key different from the one saved in the acct_profile
ensure_ssh_key:
  vultr.ssh_key.present:
    - name: idem-vultr-rsa
    - ssh_key: ~/.ssh/id_rsa_idem_vultr.pub

ensure_instance_exists:
  vultr.server.vm.present:
    - name: idem_test_vm
    - vps_plan: 16384 MB RAM,320 GB SSD,5.00 TB BW
    - os: Ubuntu 20.04 x64

ensure_baremetal_exists:
  vultr.baremetal.server.present:
    # This state is using a different `acct_profile` from the rest of the states
    - acct_profile: baremetal_profile
    - name: idem_baremetal_server
    # You can reference elements like this by their name and idem-vultr will turn it into an ID acceptable for the API
    - metalplan: 32768 MB RAM,2x 240 GB SSD,5.00 TB BW
    # You can reference elements like this by their name and idem-vultr will turn it into an ID acceptable for the API
    - os: Ubuntu 20.04 x64

ensure_loadbalancer_exists:
  vultr.loadbalancer.subscription.present:
    - name: idem_loadbalancer

ensure_object_cluster_exists:
  vultr.object_storage.subscription.present:
    - name: idem_loadbalancer
    - cluster: 2

ensure_firewall_group_exists:
  vultr.firewall.group.present:
    - name: idem_firewall_group

ensure_firewall_rule_exists:
  vultr.firewall.rule.present:
    - name: idem_firewall_rule
    # You can reference elements like this by their name and idem-vultr will turn it into an ID acceptable for the API
    - firewall_group: idem_firewall_group
    # You can reference elements like this by their name and idem-vultr will turn it into an ID acceptable for the API
    - ip_type: 4
    - protocol: icmp
    - subnet: 127.0.0.1
    - subnet_size: 24

